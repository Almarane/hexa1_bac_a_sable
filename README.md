<h1>Bonjour</h1>
<h2>Je suis un readme</h2>

<p>Pour me créer, sur votre navigateur, allez dans "Files" et mettez vous dans le dossier et la branche où vous voulez me créer. Cliquez ensuite sur le petit "+" en haut, et nommez moi "machin.md". Ce fichier servira de documentation pour GitLab et sera toujours affiché en bas de la page de mon dossier. Ce fichier ne peut cependant pas être lu avec un éditeur de texte et ne sera lisible que sur l'interface GitLab.</p>

<p>Pour me modifier, cliquez sur mon nom dans la liste des fichiers en haut, et cliquez ensuite sur "Edit".</p>

<p>Pour me mettre en page, vous pouvez me modifier comme un fichier html ;)</p>

<p>Pour récupérer ce repo, lancez Git Bash et mettez-vous dans "Mes Documents". Tapez ensuite : "git clone https://gitlab.com/Almarane/hexa1_bac_a_sable.git" (le lien qu'il y a sur la page "Projet" normalement, après avoir cliqué sur "HTTPS"). Ne vous occupez pas du lien SSH, ça marche pas du moment que ma créatrice n'a pas créé de clé SSH. Et elle a la flemme :D</p>

<p>Pour récupérer ce qui est sur le repo, faites "git pull". Normalement, Git vous demandera votre nom d'utilisateur sur GitLab, puis votre mot de passe. Notez que si rien ne s'affiche quand vous tapez votre mot de passe, c'est normal : en ligne de commande, la console n'affiche pas ce que vous tapez dans un champ "mot de passe".</p>

<p>Pour ajouter un fichier sur le repo, vous devez d'abord commit tous vos nouveaux fichiers. Pour ça, vous pouvez faire par étapes :</p>

<ul><li>Faites "git status". Il vous donnera l'état actuel des fichiers locaux par rapport au repo : soit ils sont déjà à jour, soit ils ont été modifiés, soit ce sont des nouveaux fichiers.
<li>Faites "git add <nomdufichier>" pour ajouter votre fichier à la liste des fichiers à commit. Vous pouvez aussi faire "git add --all" pour ajouter tous les fichiers qui vous ont été mentionnés dant le "git status".
<li>Faites "git commit". Vous aurez un éditeur de texte dans la console qui va s'ouvrir. Pour taper un message de commit, appuyez sur "i". <b>Vous devez impérativement mettre un message de commit, sinon votre commit échouera.</b> Quand vous avez fini, appuyez sur "Echap", puis tapez ":exit".
<li>Faites "git push". Vous allez à nouveau devoir vous identifier.
<li>Enjoy =)</ul>

<p><b>Note importante :</b> La modification d'un fichier directement sur GitLab provoque un commit. Vous devrez alors tous faire un "git pull". Si votre push échoue, essayez de faire un pull et de recommencer ;)</p>

<p>On peut aussi gérer plusieurs branches. Pour aller dans une autre branche sur le navigateur, cliquez sur la boîte "master" en haut à gauche de l'écran. Vous aurez un menu déroulant avec toutes les branches. Cliquez sur la branche que vous souhaitez voir.</p>

<p>Depuis la console, pour aller à une autre branche, vous devez taper "git checkout <nomdelabranche>". Vous pouvez faire comme ça des aller-retour entre deux branches ;)</p>

<p>Voilà, c'est à peu près tout ce qu'il y a à savoir pour une utilisation simple de Git et de GitLab. Vous pouvez vous balader dans le menu à gauche de l'écran pour voir un peu ce que GitLab propose comme services si ça vous amuse =)</p>

<p><i>Readme terminé</i></p>